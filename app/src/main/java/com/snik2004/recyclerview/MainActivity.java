package com.snik2004.recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    ArrayList<RecyclerViewItem> recyclerViewItems = new ArrayList<>();
    CardView cardView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addList();
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        adapter = new RecyclerViewAdapter(recyclerViewItems);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        cardView = findViewById(R.id.cardView);
        cardView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(this, SecondActivity.class);
//
//                intent.putExtra("position", recyclerViewItems.get(getPos).getText1());
//                intent.putExtra("text1", textView1.getText());
//                intent.putExtra("text2", textView2.getText());
//                startActivity(intent);

            }
        });

    }

    void addList() {
        for (int i = 0; i < 20; i++) {
            recyclerViewItems.add(new RecyclerViewItem(R.drawable.ic_sentiment_neutral_black_24dp, "Neutral = " + i, "Life is neutral"));
            recyclerViewItems.add(new RecyclerViewItem(R.drawable.ic_mood_black_24dp, "Mood = " + i, "Life is mood"));
            recyclerViewItems.add(new RecyclerViewItem(R.drawable.ic_sentiment_very_dissatisfied_black_24dp, "Dissatisfied = " + i, "Life is dissatisfied"));
        }
    }
}
